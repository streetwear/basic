package logx

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-kratos/kratos/v2/errors"
	klog "github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware"
	"github.com/go-kratos/kratos/v2/transport"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

var logger *Logger

type Logger struct {
	*zap.Logger
}

const (
	DebugLevel  = zapcore.DebugLevel
	InfoLevel   = zapcore.InfoLevel
	WarnLevel   = zapcore.WarnLevel
	ErrorLevel  = zapcore.ErrorLevel
	DPanicLevel = zapcore.DPanicLevel
	FatalLevel  = zapcore.FatalLevel
)

// InitLogger
// level: debug,info,warn,error,panic,fatal
func InitLogger(level zapcore.Level) {
	encoder := zapcore.NewConsoleEncoder(zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.CapitalColorLevelEncoder,
		EncodeTime:     zapcore.TimeEncoderOfLayout("[2006-01-02 - 15:04:05]"),
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	})
	core := zapcore.NewCore(encoder, os.Stdout, level)
	logger = &Logger{zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1))}
}

func GetLogger() *Logger {
	return logger
}

func (l Logger) Log(level klog.Level, keyvals ...interface{}) error {
	if len(keyvals) == 0 || len(keyvals)%2 != 0 {
		l.Warn(fmt.Sprint("Keyvalues must appear in pairs: ", keyvals))
		return nil
	}

	var data []zap.Field
	for i := 0; i < len(keyvals); i += 2 {
		data = append(data, zap.Any(fmt.Sprint(keyvals[i]), keyvals[i+1]))
	}

	switch level {
	case klog.LevelDebug:
		logger.Debug("", data...)
	case klog.LevelInfo:
		Info(keyvals)
	case klog.LevelWarn:
		logger.Warn("", data...)
	case klog.LevelError:
		logger.Error("", data...)
	case klog.LevelFatal:
		logger.Fatal("", data...)
	}
	return nil
}

func Strval(value ...interface{}) string {
	// interface 转 string
	var key []string
	if value == nil {
		return strings.Join(key, " ")
	}
	for _, v := range value {
		switch v.(type) {
		case float64:
			ft := v.(float64)
			key = append(key, strconv.FormatFloat(ft, 'f', -1, 64))
		case float32:
			ft := v.(float32)
			key = append(key, strconv.FormatFloat(float64(ft), 'f', -1, 64))
		case int:
			it := v.(int)
			key = append(key, strconv.Itoa(it))
		case uint:
			it := v.(uint)
			key = append(key, strconv.Itoa(int(it)))
		case int8:
			it := v.(int8)
			key = append(key, strconv.Itoa(int(it)))
		case uint8:
			it := v.(uint8)
			key = append(key, strconv.Itoa(int(it)))
		case int16:
			it := v.(int16)
			key = append(key, strconv.Itoa(int(it)))
		case uint16:
			it := v.(uint16)
			key = append(key, strconv.Itoa(int(it)))
		case int32:
			it := v.(int32)
			key = append(key, strconv.Itoa(int(it)))
		case uint32:
			it := v.(uint32)
			key = append(key, strconv.Itoa(int(it)))
		case int64:
			it := v.(int64)
			key = append(key, strconv.FormatInt(it, 10))
		case uint64:
			it := v.(uint64)
			key = append(key, strconv.FormatUint(it, 10))
		case string:
			key = append(key, v.(string))
		case []byte:
			key = append(key, string(v.([]byte)))
		default:
			newValue, _ := json.Marshal(v)
			key = append(key, string(newValue))
		}

	}

	return strings.Join(key, " ")
}

func Debug(args ...interface{}) {
	logger.Debug(Strval(args...))
}

func Info(args ...interface{}) {
	logger.Info(Strval(args...))
}

func Warn(args ...interface{}) {
	logger.Warn(Strval(args))
}
func Error(args ...interface{}) {
	logger.Error(Strval(args))
}
func Panic(args ...interface{}) {
	logger.Panic(Strval(args))
}
func Fatal(args ...interface{}) {
	logger.Fatal(Strval(args))
}

// KratosServerLogger 接收gin框架默认的日志
func KratosServerLogger() middleware.Middleware {
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (reply interface{}, err error) {
			var (
				code      int32
				reason    string
				kind      string
				operation string
				msg       string
			)
			startTime := time.Now()
			if info, ok := transport.FromServerContext(ctx); ok {
				info.Operation()
				kind = info.Kind().String()
				operation = info.Operation()
			}
			reply, err = handler(ctx, req)
			logInfo := fmt.Sprintf("[Kratos:%s] %3d | %12v | %-7s %s\n[request]:%s",
				kind,
				code,
				time.Since(startTime),
				operation,
				reason,
				extractReq(req),
			)
			if se := errors.FromError(err); se != nil {
				code = se.Code
				msg = se.Message
				reason = se.Reason
			}
			if err != nil {
				logInfo = fmt.Sprintf("%s[error info]:\n\t[code]:%d\n\t[reason]:%s\n\t[msg]:%s\n", logInfo, code, reason, msg)
				logger.Error(logInfo)
				return
			}
			logger.Info(logInfo)
			return
		}
	}
}

// extractArgs returns the string of the req
func extractReq(req interface{}) string {
	if stringer, ok := req.(fmt.Stringer); ok {
		return stringer.String()
	}
	return fmt.Sprintf("%+v", req)
}

// GinRecovery recover掉项目可能出现的panic，并使用zap记录相关日志
func GinRecovery(stack bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic stack trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}
				httpRequest, _ := httputil.DumpRequest(c.Request, false)
				if brokenPipe {
					logger.Logger.Error(
						fmt.Sprintln(
							c.Request.URL.Path,
							zap.Any("error", err),
							zap.String("request", string(httpRequest)),
						),
					)
					// If the connection is dead, we can't write a status to it.
					c.Error(err.(error)) // nolint: errcheck
					c.Abort()
					return
				}

				if stack {
					logger.Error(
						fmt.Sprintln(
							"[Recovery from panic]",
							zap.Any("error", err).String,
							zap.String("request", string(httpRequest)).String,
							zap.String("stack", string(debug.Stack())).String,
						),
					)
				} else {
					logger.Logger.Error(
						fmt.Sprintln(
							"[Recovery from panic]",
							zap.Any("error", err),
							zap.String("request", string(httpRequest)),
						),
					)
				}
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}()
		c.Next()
	}
}
