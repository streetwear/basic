package httpx

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Post url:完整路径，body：json结构体，param：Get参数，Header：请求头
func Post(url string, body interface{}, params map[string]string, headers map[string]string) ([]byte, error) {
	//add post body
	var bodyJson []byte
	var req *http.Request
	if body != nil {
		var err error
		bodyJson, err = json.Marshal(body)
		if err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(bodyJson))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-type", "application/json")
	//add params
	q := req.URL.Query()
	if params != nil {
		for key, val := range params {
			q.Add(key, val)
		}
		req.URL.RawQuery = q.Encode()
	}
	//add headers
	if headers != nil {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}
	//http client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	respStr, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return respStr, nil
}

// Get url:完整路径，param：Get参数，Header：请求头
func Get(url string, params map[string]string, headers map[string]string) ([]byte, error) {
	//add post body
	var bodyJson []byte
	var req *http.Request
	req, err := http.NewRequest(http.MethodGet, url, bytes.NewBuffer(bodyJson))
	if err != nil {
		return nil, err
	}
	//add params
	q := req.URL.Query()
	if params != nil {
		for key, val := range params {
			q.Add(key, val)
		}
		req.URL.RawQuery = q.Encode()
	}
	//add headers
	if headers != nil {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}
	//http client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	respStr, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return respStr, nil
}
