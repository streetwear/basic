module gitlab.com/streetwear/basic

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/go-kratos/kratos/v2 v2.2.0
	go.uber.org/zap v1.20.0
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.1
)
