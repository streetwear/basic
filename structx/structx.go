package structx

type PageResult struct {
	Count     int64 `json:"count"`
	Page      int   `json:"page"`
	Row       int   `json:"row"`
	PageCount int64 `json:"pageCount"`
}

type PageQuery struct {
	Page int `form:"page" binding:"required,gte=1"`
	Row  int `form:"row" binding:"required,gte=1"`
}
