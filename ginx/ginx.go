package ginx

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"time"
)

type ResponseWriter struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

func (w ResponseWriter) Write(b []byte) (int, error) {
	w.Body.Write(b)
	return w.ResponseWriter.Write(b)
}

func (w ResponseWriter) WriteString(s string) (int, error) {
	w.Body.WriteString(s)
	return w.ResponseWriter.WriteString(s)
}

type WriteLog func(c *gin.Context, blw *ResponseWriter, paramStr string)

type LoggerInfo struct {
	ID         int64     `json:"id"`
	Userid     int64     `json:"userid"`
	Username   string    `json:"username"`
	Result     int       `json:"result"`
	ErrMsg     string    `json:"errmsg"`
	IP         string    `json:"ip"`
	Params     string    `json:"params"`
	URL        string    `json:"url"`
	CreateTime time.Time `json:"createtime"`
}

func Logger(writeLog WriteLog) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 拦截响应数据
		blw := &ResponseWriter{Body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw
		//
		paramStr := ""
		if "application/json" == c.ContentType() {
			p := make(map[string]interface{}) //注意该结构接受的内容
			_ = c.ShouldBindBodyWith(&p, binding.JSON)
			b, _ := json.Marshal(p)
			paramStr = string(b)
		}
		c.Next()
		// 操作日志
		go writeLog(c, blw, paramStr)
		c.Next()
	}
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "POST, GET")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		// 处理请求
		c.Next()
	}
}
