package jwtx

import (
	"github.com/dgrijalva/jwt-go"
	"strings"
)

var Jwt *JwtClaims

type JwtClaims struct {
	Userid   int    `json:"userid"`
	Username string `json:"username"`
	jwt.StandardClaims
	secretKey string
	seconds   int64
}

func InitJwt(AccessSecret string, AccessExpire int64) {
	Jwt = &JwtClaims{
		secretKey: AccessSecret,
		seconds:   AccessExpire,
	}
}

func (j *JwtClaims) GenToken(iat int64, payloads map[string]interface{}) (string, error) {
	claims := make(jwt.MapClaims)
	claims["exp"] = iat + j.seconds
	claims["iat"] = iat
	for k, v := range payloads {
		claims[k] = v
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = claims

	return token.SignedString([]byte(j.secretKey))
}

func (j *JwtClaims) GetClaimsMap(authorization string) jwt.MapClaims {
	token := strings.ReplaceAll(authorization, "Bearer ", "")
	user, _ := jwt.Parse(token, func(token *jwt.Token) (i interface{}, e error) {
		return j.secretKey, nil
	})
	if user == nil {
		return nil
	}
	claims := user.Claims.(jwt.MapClaims)
	return claims
}
