package ormx

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

const (
	Silent = logger.Silent
	Error  = logger.Error
	Warn   = logger.Warn
	Info   = logger.Info
)

func MysqlConnect(addr string, level logger.LogLevel) *gorm.DB {
	db, err := gorm.Open(mysql.Open(addr), &gorm.Config{
		Logger: logger.Default.LogMode(level),
	})
	if err != nil {
		panic("failed to connect database")
	}
	return db
}
