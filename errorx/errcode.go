package errorx

const (
	StatusUnauthorized = 401        //认证失败
	ErrorUnknown       = iota + 500 //未知错误
	ErrorTimeOut                    //请求超时
	ErrorParam                      //参数错误
	ErrorPwd                        //
	ErrorNoPower                    //无操作权限
	ErrorTimeFormat                 //日期格式错误
	ErrorProcessMsg                 //映射关系错误
	ErrorNatsConn                   //消息中心连接错误
	ErrorDataFormat                 //数据格式转换错误
	ErrorUserChanged                //用户变更
	ErrorNotExt                     //数据不存在
	ErrorLoginAccount               //账户或密码错误
	ErrorInsert                     //插入失败
	ErrorUpdate                     //更新失败
	ErrorDelete                     //删除失败
	ErrorQuery                      //查询失败
	ErrorOrderWrong                 //顺序错误
	ErrorFailed                     //错误
	ErrorFile                       //错误文件
	ErrorEqInit                     //初始化失败
)

var ErrorMap = map[int]string{
	StatusUnauthorized: "认证失败",
	ErrorUnknown:       "未知错误",
	ErrorTimeOut:       "请求超时",
	ErrorParam:         "参数错误",
	ErrorPwd:           "密码错误",
	ErrorNoPower:       "无操作权限",
	ErrorTimeFormat:    "日期格式错误",
	ErrorProcessMsg:    "映射关系错误",
	ErrorNatsConn:      "消息中心连接错误",
	ErrorDataFormat:    "数据格式转换错误",
	ErrorUserChanged:   "用户变更",
	ErrorNotExt:        "数据不存在",
	ErrorLoginAccount:  "账户或密码错误",
	ErrorInsert:        "新增失败",
	ErrorUpdate:        "修改失败",
	ErrorDelete:        "删除失败",
	ErrorQuery:         "查询错误",
	ErrorOrderWrong:    "指令错误",
	ErrorFailed:        "操作失败",
	ErrorFile:          "缺少参数文件",
	ErrorEqInit:        "设备初始化错误",
}

type ResultInfo struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}
