package errorx

func ErrorCode(code int) *ResultInfo {
	return &ResultInfo{
		Code:    code,
		Message: ErrorMap[code],
	}
}

func ErrorErrorMsg(code int,msg string)*ResultInfo{
	return &ResultInfo{
		Code:    code,
		Message: msg,
	}
}

func ErrParam() *ResultInfo {
	return &ResultInfo{
		Code:    ErrorParam,
		Message: ErrorMap[ErrorParam],
	}
}

func Success(data interface{}) *ResultInfo {
	return &ResultInfo{
		Code:    0,
		Message: "OK",
		Data:    data,
	}
}
